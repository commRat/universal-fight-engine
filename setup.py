#!/usr/bin/python

from setuptools import setup, find_packages
import pathlib
from ufe.version import __version__


here = pathlib.Path(__file__).parent.resolve()
long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name="ufe",
    version=__version__,
    description="Python library which simulates fight between two objects",
    long_description=long_description,
    url="https://gitlab.com/commRat/universal-fight-engine",
    author='David Tomicek',
    author_email='tomicek.david@yahoo.com',
    packages=find_packages(),
)
