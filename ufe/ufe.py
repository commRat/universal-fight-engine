import time
from randombase import randombase


class Player:

    def __init__(self, name, hp, dmg_min, dmg_max, speed, critical_chance):
        self.name = name
        self.hp = hp
        self.dmg_min = dmg_min
        self.dmg_max = dmg_max
        self.speed = speed
        self.critical_chance = critical_chance

    def attack(self):
        return randombase.random(self.dmg_min, self.dmg_max)

    def defend(self, dmg_recieved):
        self.hp -= dmg_recieved
        return self.hp

    def critical_hit(self):
        if self.critical_chance < 1 or self.critical_chance > 100:
            return False

        random_num = randombase.random(1, 100)
        return True if random_num <= self.critical_chance else False


def who_starts(player1, player2):
    if player1.speed != player2.speed:
        return player1 if player1.speed > player2.speed else player2

    return randombase.choose_obj(player1, player2)


def fight(player1, player2):
    attacker = who_starts(player1, player2)
    defender = player1 if player2 == attacker else player2
    round_number = 0
    metadata = {
        "init": {
            "attacker_name": attacker.name,
            "attacker_dmg_min": attacker.dmg_min,
            "attacker_dmg_max": attacker.dmg_max,
            "attacker_hp": attacker.hp,
            "attacker_speed": attacker.speed,
            "defender_name": defender.name,
            "defender_dmg_min": defender.dmg_min,
            "defender_dmg_max": defender.dmg_max,
            "defender_hp": defender.hp,
            "defender_speed": defender.speed,
            "start_time": time.time(),
        },
        "rounds": {},
    }
    rounds_k = metadata.get("rounds")

    while 1:
        t = time.time()
        round_number += 1
        round = f"round{round_number}"

        attacker_critical_hit = attacker.critical_hit()
        attacker_dmg = attacker.attack() * 2 if attacker_critical_hit else attacker.attack()
        defender_hp = defender.defend(attacker_dmg)
        rounds_k[round] = {
            "attacker_dmg": attacker_dmg,
            "attacker_critical_hit": attacker_critical_hit,
            "defender_hp_remaining": defender_hp,
            "t": t,
        }
        if defender_hp <= 0:
            metadata["result"] = {
                "winner": attacker.name,
                "defeated": defender.name,
                "end_time": t,
            }
            return metadata

        defender_critical_hit = defender.critical_hit()
        defender_dmg = defender.attack() * 2 if defender_critical_hit else defender.attack()
        attacker_hp = attacker.defend(defender_dmg)
        rounds_k[round].update({
            "defender_dmg": defender_dmg,
            "defender_critical_hit": defender_critical_hit,
            "attacker_hp_remaining": attacker_hp,
        })
        if attacker_hp <= 0:
            metadata["result"] = {
                "winner": defender.name,
                "defeated": attacker.name,
                "end_time": t,
            }
            return metadata
